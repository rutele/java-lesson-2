package lt.vcs;
import java.util.Scanner;

public class Main {

    private static void out(String txt) {
        System.out.println(txt);
    }
    
    private static void out1(String txt) {
        System.out.print(txt);
    }
    
    private static Scanner newScan(){
        return new Scanner(System.in); //grąžinti naują scanner objektą
    }
     
    private static int inInt(){
        return newScan().nextInt(); //priimti sveiką skaičių
    }
    
    private static double inD(){
        return newScan().nextDouble(); //priimti sveiką skaičių
    }
    
    private static String inWord(){
        return newScan().next();
    }
    
    private static void task1(){
        out("Please enter 5 integers");
        int [] penki = new int[5];
        penki[0] = inInt();
        penki[1] = inInt();
        penki[2] = inInt();
        penki[3] = inInt();
        penki[4] = inInt();

        int sum = 0;
        for(int i=0; i<5; i++){
            sum += penki[i];
            out1("" + penki[i]);
            if(i==4){break;}
            out1(" + ");
        }
        out(" = "+sum);
    }
    
    private static void task2(){
        out("Please enter 5 words");
        String [] penki = new String[5];
        penki[0] = inWord();
        penki[1] = inWord();
        penki[2] = inWord();
        penki[3] = inWord();
        penki[4] = inWord();
        
        int j = 0;
        for(j=0; j<5; j++){
            out1(penki[j] + " ");
            if(j==4){break;}
        }
        out("");
    }
 
    private static void task3(){
        out("Please enter your height in m");
        double H = inD();
        out("Please enter your weight in kg");
        double W = inD();
        double BMI = W/(H*H);
        BMI = Math.round(BMI *100.0)/100.0;
        out("Your BMI is " + BMI);
        
    }
    
    private static void task4(){
        out("How many entries there will be?");
        int entries = inInt();
        int [] numEntries = new int[entries];
        int i = 0;
        for(i=0; i<entries; i++){
            numEntries [i] = inInt();
            }
        out("Numbers higher than 100: ");
        for(i=0; i<entries; i++){
            if (numEntries [i] > 100){
                out1(numEntries[i] + " ");
                out("");
            }
        }  
    }

    public static void main(String[] args) {
        out("Please choose an app:");
        out("1 - number sum");
        out("2 - word sum");
        out("3 - Body mass index");
        out("4 - higher than 100");
        int task = inInt(); 
        switch (task){
            case 1:
                Main.task1();
                break;
            case 2:
                Main.task2();
                break;
            case 3:
                Main.task3();
                break;
            case 4:
                Main.task4();
                break;
            default:
                out("Entry not valid");
                break;
        }
    } 
}
