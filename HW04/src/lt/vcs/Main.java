package lt.vcs;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import static lt.vcs.VcsUtils.*;

public class Main {

    public static void task1(String str) {
        str = str.replaceAll(" ", ""); //tarpelių ištrynimas
        String isPalindrome = new StringBuilder(str).reverse().toString();
        out(isPalindrome);
        if (str.equals(isPalindrome)){
            out("It is a palindrome!");
        }
        else{
            out("It is NOT a palindrome!");
        }
    }

    public static void task2() {
        List<Integer> list1 = new ArrayList(Arrays.asList(2, 5, 1, 9, -999, 9879, 55, 26));
        out("" + "Sąrašas: " + list1);
        out("Sąrašas didėjančia tvarka: ");
        Collections.sort(list1);
        for (int i = 0; i < list1.size(); i++) {
            System.out.print("" + list1.get(i) + " ");
        }
    }

    public static void task3() {
        List<Integer> list1 = new ArrayList(Arrays.asList(2, 5, 1, 9, -999, 9879, 55, 26));
        out("" + "Sąrašas: " + list1);
        out("Sąrašas mažėjančia tvarka: ");
        Collections.sort(list1, Collections.reverseOrder()); //arba Collections.reverse(...)
        for (int i = 0; i < list1.size(); i++) {
            System.out.print("" + list1.get(i) + " ");
        }
    }

    
    public static void main(String[] args) {
       out("Please choose an app:");
        out("1 - palindromai");
        out("2 - sort skaičius didėjančia");
        out("3 - sort skaičius mažėjančia");       
        int task = inInt(); 
        switch (task){
            case 1:
                task1(inLine("Enter a probable palindrome: "));
                break;
            case 2:
                task2();
                break;
            case 3:
                task3();
                break;
            default:
                out("Entry not valid");
                break;
        }
    }  
}
