package lt.vcs;
// import skiltis, jei norime importuoti klasę iš kito projekto

public class Main { 
//vienam faile - tik viena public klasė. 
//vadinasi taip pat, kaip ir .java failo pavadinimas
    
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }
    
//===== BAZINĖ KLASĖS STRUKTŪRA ================================================    
    //KINTAMIEJI, pvz:
    //private final Point p1;
    
    //KONSTRUKTORIAI, pvz:
    //public Main(Point p1){
    //this.p1 = p1;
    //this.distance = .......;}
    
    //ANOTACIJOS, pvz:
    //@Override
    //public String toString(){  return ""+p1;}
    
    //METODAI
    
    //GET SET METODAI, pvz:
    //public Point getP1(){ return p1;}
    
    
//===== KOMENTARAI =============================================================
    // 1 tipas - single line
    // comment
    
    // 2 tipas - multi line
    /*
     * comment
     */
    
    // 3 tipas - JavaDocs, skirtas apibūdinti metodams. Užvesk pelę ant Main.
    /**
     * Čia yra pagrindinė klasė su main metodu
     * @param args apibūdintas metodas
     */
   
//===== BAZINĖ KLASĖS STRUKTŪRA ================================================ 
    //java is case sensitive. use CamelCase!
        //KlasėsVardas (Interface, Enum, Constant - iš didžiosios)
        //kintamojoVardas
        //metodoVardas()
    //program file name turi sutapti su klasės pavadinimu
    
//===== JAVA MODIFIKATORIAI ====================================================
    //Prieinamumo (saugumui užtikrinti):
        //public        viešai prieinamas iš visur
        //protected     gali prieiti klasės, kurios extendina iš Main klasės
        //default       (jei nenurodyta) protected, esantys tame pačiame pakete
        //private       pasiekiamas tik klasės viduje

    //Neprieinamumo:
        //final        priskiriama reikšmė kintamajam, negali būti pakeista
        //abstract     neturi {...}. Klasė, kuri extendina, automatiškai turės jį.
        //static       pasiekiamas nesukuriant objekto, tiesiai per klasę.
        //strictfp     suvienodina veikimą tarp skirtingų Java versijų
    
    //e.g. private static final String kazkoksKintamasis = "Nekeičiama reikšmė";
    //jei klasė turi bent vieną abstract metodą, klasė turi būti irgi abstract
    //static negali eiti kartu su abstract
    //Main klasėsObjektas = new Main();  - Main() yra klasės Main konstruktorius.
    //static metodas gali naudoti tik static narius/kintamuosius  
    
//===== JAVA KINTAMIEJI ========================================================
    //Klasės - static
    //Metodo - non-static
    //Lokalūs - <neturi modifikatoriaus> yra pvz "if" ribose. gali būti nieko arba final
    
//===== DUOMENŲ TIPAI ==========================================================
    //Primityvūs (in stack memory) greičiau pasiekiami
    //Žymima mėlynai, iš mažosios
        //Numeric:
            //Integers (byte - 8 bits, short - 16, int - 32, long - 64 (124L) )
                //2^(bits-1), e.g. 2^8 bits can contain [-127; 127} characters
            //Floating point - float (123.4F), double (123.4D)
        //Character ('k')            
        //Boolean
    
    //Objektiniai(adresas in stack, o nurodoma į heap)- visi klasių kintamieji
    //Žymima juodai, iš didžiosios
    
    // void - nieko negrąžina
    //jei grąžina, turi žodį return
    
//===== JAVA IDENTIFIKATORIAI ==================================================
    //Prasideda lotyniškomis raidėmis arba $ arba _
    //po pirmojo simbolio - bet kokie, išskytus tašką
    //negali sutapti su Java raktiniais žodžiais
    
//===== OPERATORIAI ============================================================
    //Priskyrimo: =, +=, -=, *=, /=
    //Aritmetiniai: +, -, *, /, %, ++, -- 
    //Palyginimo: <, <=, >, >=, ==, !=  (objektinius kintamuosius lyginti su x.equals(y)
    //Loginiai: !, &&, ||
 
//===== SĄLYGOS ================================================================
    //if, else if, else
    //switch(x) case a: break; default;
    //foreach rašomas taip:
        //String[] mass = {"", "", ""};
        //for(String elementas : mass){}   
}
