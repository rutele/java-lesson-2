//NEATLIKTA UŽDUOTIS

package lt.vcs;
import static lt.vcs.VcsUtils.*;

//VcsUtils - pagalbiniai įvesties/išvesties/random metodai
//Combination - pokerio kombinacijos enum'as = stiprumas 1-7, bonusai
//HandUtils - kada būna kokia kombinacija
//Hand
//GameUtils - išridena kauliuką/penkis kauliukus, išsprendžia lygiąsias
//Game - sumeta vieną partiją dviems žaidėjams
//Player - apibrėžia žaidėją



public class Main {

    public static void main(String[] args) {
        out("===============Kauliuku pokeris==================");
        String p1name = inWord("Player 1, please enter your name");
        String p2name = inWord("Player 2, please enter your name");
        
        Player p1 = new Player(p1name);
        Player p2 = new Player(p2name);
        boolean zaisti = true;
        
        while (zaisti) {
            out("Both players have 250 EUR each.");
            
            //p1 pasirinkti statymo sumą
            int suma1 = inInt(""+ p1name+ ", please enter your statymo suma");
            //p2 išlyginti arba pakelti
            int suma2 = inInt(""+ p2name+ ", please enter your statymo suma");
            
            //repeat kol susilygins suma
            //
            //
            //
            
            
            //išridenami kauliukai
            Game newGame = new Game(p1, p2);
            Player nugaletojas = newGame.start();
            Player pralaimetojas = getPralaimetojas(newGame, nugaletojas);
            
    //Metodas laimetojui nustatyti, kaip parametrus priima 
    //zaideju objektus, zaideju objektai turi savyje rankos objektus kuriame laikomos
    //isridentu kauliuku reiksmes, pagal jas metodas ir apskaiciuoja kombinacija ir 
    //laimetoja. Priklausomai nuo kombinacijos laimetojas tiesiog gauna bonus laimejima. 
    //Statomos sumos minusuojamos is abieju zaideju laimetojas pasiima pastatyta suma 
    //ir jei yra bonus laimejima.
    //
    //
    //
    
            out(nugaletojas.getName() + ", in your account you have: " + nugaletojas.getCash() + " EUR");
            out(pralaimetojas.getName() + ", in your account you have: " + pralaimetojas.getCash()+ " EUR");
            
        
    //Zaidimas kartojamas kol zaidejei nenuspredzia baigti zaidima arba kazkuris 
    //zaidejas pritruksta pinigu :)

           //setCash(); 
 
            int choice = inInt("ka norite daryti toliau? 0-zaisti dar; 1-baigti zaidima");
            if (choice == 1) {
                break;
            }
        }
        
    }
    
    private static Player getPralaimetojas(Game game, Player winner) {
        if (winner.equals(game.getP1())) {
            return game.getP2();
        } else {
            return game.getP1();
        }
    }
    
    public static Player getNextActivePlayer(Game game) {
        if (game.getActivePlayer().equals(game.getP1())) {
            return game.getP2();
        } else {
            return game.getP1();
        }
    }
    
    private static int rollDice() {
        return random(1, 6);
    }
    
    /**
     * perridena norimus kauliukus ir perskaiciuoja kombinacija ir kitus skaicius
     * @param dices kauliuku skaiciai, atskirti kableliu, kuriuos norim perridenti
     */
    public static void reRollDice(int[] hand, String dices) {
        dices = dices.replaceAll(" ", "");
        for (String dice : dices.split(",")) {
            Integer nr = new Integer(dice);
            hand[nr - 1] = rollDice();
        }
    }
}


/*
2*) pasunkintas variantas:
po to kai zaideju kamuoliukai isridenami, ir po to kai jus atvaizduojat kas 
iskrito, zaidejui pradejusiam statyma leisti jei jis nori pakelti jau esama 
pastatyta suma dar karta, antram zaidejui vel reikia rinktis ar jis nori islyginti 
ar pakelti ar pralaimeti jau pastatyta suma; jei/kai sumos islygintos kiekvienam 
zaidejui leisti pasirinkti kuriuos kauliukus is jau jam iskritusiu jis noretu 
perridenti 1 karta, jei tik jis nori perridenti; Kai/jei abu zaidejei baige 
perridenti savo kauliukus tik tada nustatomas laimetojas :)
*/