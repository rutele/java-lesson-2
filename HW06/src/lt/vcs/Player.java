package lt.vcs;

public class Player {
    
    //KINTAMIEJI
    private String name;
    private int cash = 250;
    private Hand hand;

    //KONSTRUKTORIUS - player turi vardą ir hand
    public Player(String name) {
        this.name = name;
        this.hand = new Hand(GameUtils.rollHand());
    }

    // GET/SET METODAI
    public String getName() {
        return name;
    }

    public int getCash() {
        return cash;
    }
    public void setCash(int cash) {
        this.cash = cash;
    }
    public String getStats() {
        return name + "(cash: " + cash + ")";
    }

    public Hand getHand() {
        return hand;
    }
    public void setHand(Hand hand) {
        this.hand = hand;
    }
}
