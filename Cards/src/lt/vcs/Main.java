//NEPADARYTOS UŽDUOTYS:
//HW5 - 2 (letter count) pagal skaičių
//HW6 - kauliukų pokeris
//bonus - kortos


package lt.vcs;


public class Main {

    
    public static void main(String[] args) {
        
    }
    
}
/*
(30 x *)Papildoma tesiama užduotis/programa:
Susikurti naują projektą "kortos", su standartine lt.vcs.Main klase ir į projektą 
nusikopijuoti naujausias VcsUtils, Person, Gender ir Player klases;
Susikurti naują klasę lt.vcs.Korta; Klasės struktūra:
private String simbolis; //laikome kortos skaiciu(nuo 2 iki 10) ar raide(J, Q, K , A).
private Zenklas zenklas;
sitie kintamieji privalomi, todėl jums reikės konstruktorio, kuris juos būtinai 
užpildytų; Papildomai klasė gali turėti kokių tik norite dalykų, jums spręsti, 
ko jums gali prireikti, nepamirskite tik kintamuju get metodu. Papildomai galite
prisideti kokiu ir kiek tik norite kintamuju, konstruktoriu, metodu ir t.t.
Susikurti naują enum klasę lt.vcs.Zenklas; enum'o reikšmės:
K,V,B,S; //(K - kryziai, V - vynai, B - bugnai, S - sirdys)
galite prisidėti kiek tik norite klasės lygio kintamuju ir juos užsipildyti 
naudojant konstruktorių, bei nepamirškite jiem get metodų. Viskas paliekama jusu 
vaizduotei ir poreikiams ;)
Susikurti naują klasę lt.vcs.KortuUtils; klasė bus skirta pagalbiniams metodams 
su kortomis, klasės struktūra:
public static List<Korta> sukurtiKalade(){...} //metodas skirtas sukonstruoti 
kortu kaladę su kortomis nuo 2 iki tūzo(A), zinoma visu zenklu po viena, viso 52 
kortos.
public static void maisyti(List<Korta> kortos) {
if (kortos != null) {
Collections.shuffle(kortos);
}
}//metodas ismaisyti kortas
Toliau galite pagal poreikius ir savo vaizduote kurtis kokias tik norit klases, 
kokius norit metodus ir tobulinti esamas klases kaip jum kyla poreikiai ar norai 
ar būtinybė.

Projekte kursite skirtingus kortų žaidimus, kiekvienas žaidimas turės savo klasę, 
ir kiekviena žaidimo klasė turės start() metodą žaidimui pradėti. Naudotojams 
leisite pasirinkti kokį žaidimą jie nori žaisti, atatinkamai nuo žaidimo skirsis 
ir žaidėjų kiekis reikalingas žaidimui.
Pvz. lt.vcs.Main.main metode:
Zaidimas zaid = new Zaidimas();
zaid.start();
//nuo jusu priklausys kokius konstruktorius darysite, kaip kursite.

1) Sukurti klasę lt.vcs.Karas; Klasė privalo turėti metodą:
public void start() {...}//metodas kur prasidės kortu zaidimas "karas". Žaidimas 
skirtas 1 žaidėjui, žaisti prieš kompiuterį.
Žaidėjui duodamas tik vienas veiksmas dėti kortą, čia kad automatizuotai per pora 
sekundžių nesusižaistų automatiškai partija :) Priklausomai nuo to ar korta kurią
padeda žaidėjas ar jo priešininkas atversta ar užversta informuoti apie tai kokia
ten korta ir kaip vyksta žaidimo eiga.
Žaidimo taisyklės: https://lt.wikipedia.org/wiki/Karas_(kort%C5%B3_%C5%BEaidimas)
 
 2) a) kortu zaidimas ragana(arba piku dama):
nauja klase lt.vcs.Ragana; turi tureti metoda start();
zaidimas 2 zaidejam;
Kai zaidejo eile traukti korta pasakoma kiek kortu turi zaidejas ir paklausiama, kuria norima traukti.
Zaidimo taisykles: http://www.kortuzaidimai.lt/piku-dama

2) b*) leisti pasirinkti kiek zaideju nori zaisti si zaidima, minimum gali zaisti 2 zaidejei, pradedamas zaidimas nuo pirmojo zaidejo eiles tvarka traukiamos kortos 

po to kai pasirenkamas zaidimas, ir jei zaidime pasirenkama kiek naudotoju gali ji zaisti, paprasyti naudotojo pasirinkti, ar jis nori uzsiregistruoti ar prisijungti.
Visus naudotoju duomenis saugoti failuose, kazkokioje konstanta esancioje pagrindineje direktorijoje, pvz.:
C:\temp\kortos tureti atskira folderi users, kur saugoti tekstiniuose failuose naudotojo informacija; Kiekvienas naudotojas turi tureti savo faila tokiu pat pavadinimu kaip jo email'as, todel emailai turi buti unikalus, pvz.: mano@email.as.txt
faile turi buti visi naudotojo duomenys saugomi, kurie nurodyti Player klases hierarchijoje, tokiu formatu - <laukoPavadinimas>=<'laukoReiksme'> , pvz.:
password='uzkoduotasSlaptazodis'
name='Vardenis'
surname=null
lytis=1
email='mano@email.as'
........

Kai naudotojai prisijungia, tikrinti pagal uzkoduota slaptazodi, tada Player objektus programoje konstruoti is informacijos saugomos naudotoju failuose
*/

