package lt.vcs;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import static lt.vcs.VcsUtils.*;

public class Main {

    private static final String PVZ_FAILAS = "C:\\Users\\User\\Documents\\Java\\pvz.txt";
    private static final String TEMP_DIR = "C:\\Users\\User\\Documents\\Java";
    private static final String UTF_8 = "UTF-8";
    
    //============= CREATE A FILE IN A DIRECTORY ==========================
    
    private static File newFile(String dir, String fileName){
        File result = null;
        if(dir == null || fileName == null){
            out("Your entry is not valid");
        }
        else{
            File file = new File(dir, fileName);
            if (file.isDirectory()){
                if (! file.exists()) {       
                    try {        
                        file.mkdirs();
                    } catch (Exception e) {
                        out(e.getMessage());
                    }
                }
                result = new File(file, fileName);
                     try {        
                        result.createNewFile();
                    } catch (Exception e) {
                        out(e.getMessage());
                    }   
            }
            else{
            out("Your entered directory is not valid") ;   
            }
        }                         
                             
    return result;
    }
        
                    
    public static void main(String[] args) {
        
    File pvzFile = new File(PVZ_FAILAS);
    
    
    //======================= READ FILES =======================================
    
    BufferedReader br = null;
    String failoTekstas = "";
    String line;
    try{
        br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile), UTF_8));
                
        while ((line = br.readLine()) != null) { //null būna, kai yra paskutinė failo eilutė ir po jos nėra nieko
            failoTekstas += line + System.lineSeparator(); // tekstas+ perkelia į naują eilutę 
            out(line);                           //atvaizduoja kiekvieną eilutę iš failo
        }
    } catch(Exception e){
        out(e.getMessage());
    } finally{                  //vykdomas bet kokiu atveju, nesvarbu, ar pavyko, ar ne
       if (br != null){
           try{
                br.close();
           } catch (Exception e){
              out(e.getMessage()); 
           }     
       } 
    }
    
    //======================= WRITE FILES =======================================
        
    BufferedWriter bw = null;
    try {
        
        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pvzFile), UTF_8));
//         //save
//        bw.write("raso I faila");
//        bw.newLine(); //nauja eilute
        bw.append(failoTekstas);   
        bw.append("Prijungiame ketvirtą eilutę");
        bw.flush(); //nuflushina pakeitimus, išsaugo
    } catch (Exception e) {
        out(e.getMessage());
    } finally {                  //vykdomas bet kokiu atveju, nesvarbu, ar pavyko, ar ne
        if (bw != null) {
            try {
                bw.close();
            } catch (Exception e) {
                out(e.getMessage());
            }
            }
        }
    }
}

//============================DARBAS SU FAILAIS=================================
// Javoje dirbama pagalbinėje klasėje Java.io (input-output) arba Java.nio
// Stream (srautai) naudojami kuriant naujus Stream klasės objektus:
//      InputStream (for input), OutputStream (for output), 
//      dar yra ByteStream (tekstiškai neskaitomi failai - audio, video, foto)
//      CharacterStream - teksto redaktoriumi atidaromi failai
//       pagrindinė klasė - klasė File
// ir input, ir output streamus reikia uždaryti baigiant darbą.

//pvzFile. metodai:
//canExecute() - ar programa gali paleisti failą (pvz .exe), boolean
//canRead() - ar gali perskaityti failą
//canWrite() - ar gali įrašyti į failą
//createNewFile() - sukuria failą fiziškai, jei jis dar neegzistuoja
//delete() - ištrina failą, boolean ar pavyko
//deleteOnExit() - prieš uždarant programą ištrina failą
//equals(Object obj) - palygina kelią iki failo
//exists() - ar jau yra toks failas fiziškai?
//getName() - grąžina pavadinimą failo arba direktorijos
//getParent() - paskutinį failo direktorijos pavadinimą
//isDirectory() - grąžina boolean, ar direktorija teisinga
//isFile() - ar direktorija teisinga
//isHidden() - ar failas paslėptas
//list() - gražina string masyvą, užpildytą direktorijomis iš parent folderio
//listFiles() - grąžina file objektų masyvą,išfiltuotų failų sąrašą iš direktorijos
//mkdir() - sukuria paskutinę direktoriją, boolean ar pavyko. failina, jei nėra folderio prieš jį
//mkdirs() - sukuria visą kelią iki nurodytos direktorijos, boolean ar pavyko
//renameTo() - perkelia failą į kitą vietą arba pervadina failą, bet direktorija jau turi egzistuoti
//setReadOnly()
//toPath() - grąžina path objektą
//toString() - grąžina pathname, kurį ir padavėme į konstruktorių

//@Deprecated - metodas yra pasenęs, negarantuoja veikimo

//Files. metodai (papildomi) iš nio.file.Files - daug geresni, nes platform independent
//copy(from, to) 
//createDirectory() - kaip mkdir()
//createDirectories() - kaip mkdirs()
//createFile() = kaip createNewFile()
//createTempFile() - sukuriamas failas programos veikimo laikotarpiu, bet jei crashina programa, failai gali neišsitrinti
//delete()
//exists()
//deleteIfExists()
//file() - failai ieškomi failų sistemoje
//getAttribute() - 
//list() grąžina Stream<Path>



