package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

public class Main {
    
        public static final String SELECT_LAST_ID_SQL = "select MAX(id) from %s "; //string formater
        static String url = "jdbc:mysql://localhost:3306/";
        static String dbName = "vcs_2017";
        static String userName = "root";
        static String password = "";   
        
    private static Integer getLastId(String table){
        Integer result = null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement s = conn.createStatement();
            //ResultSet rs = s.executeQuery("select id from " + table + " order by id desc limit 1;");
            //arba naudoti prepared statements:
            ResultSet rs = s.executeQuery(String.format(SELECT_LAST_ID_SQL, table));
            
            
            if(rs.next()){
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            out("Error while retrieving the last id from " + table + ": " + e.getMessage());
            throw new RuntimeException (e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                    throw new RuntimeException(e);
                }
            }
        }
        return result;
    }
    
    private static void outCarResultSet(ResultSet rs) throws Exception {
        rs.beforeFirst();  //atstato pointerį į pradžią
        while (rs.next()) {
                out("id='"+rs.getInt(1)+"' make='"+rs.getString(2)+"' name ='"+rs.getString(3)+"'");
            }
        rs.beforeFirst();
    }
    
    public static void main(String[] args) {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            
            Statement s = conn.createStatement(
                ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = s.executeQuery("select * from car");
            
//            out("=========== CAR lentelės turinys prieš UPDATE =================");
//            outCarResultSet(rs);

//            for(int i=1; rs.next(); i++){                                      
//                rs.updateString("make", rs.getString("make") + " (kazka kompensuoja) " + i);
//                rs.updateRow();  //padarius pakitimą, norint kad jis atsigultų į DB, reikia updeitint ROW.
//            }
//            rs.beforeFirst();   
//            out("=========== CAR lentelės turinys po UPDATE ================="); 
//            outCarResultSet(rs);
            
            
            out("=========== CAR lentelės turinys prieš INSERT =================");
            outCarResultSet(rs);
            
            rs.moveToInsertRow();
            rs.updateInt(1, getLastId("car") + 1);
            rs.updateString("make", "ZAPUKAS");
            rs.updateString("name", "sens bet gers ir raudons");
            rs.insertRow();
            out("=========== CAR lentelės turinys po INSERT ================="); 
            outCarResultSet(rs);
            
            rs.afterLast();
            rs.previous();
            rs.deleteRow();
            out("=========== CAR lentelės turinys po DELETE ================="); 
            outCarResultSet(rs);
            
        } catch (Exception e) {
            out("Unable to reach the database: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
