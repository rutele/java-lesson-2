package lt.vcs;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

public class Main {

    
    private static Integer getLastId(Statement s, String table){
        Integer result = null;
        try {
            ResultSet rs = s.executeQuery("select id from " + table + " order by id desc limit 1;");
            //arba "select MAX(id) from" + table;"
            if(rs.next()){
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            out("Error while retrieving the last id from " + table + ": " + e.getMessage());
        }
        return result;
    }
    
             
    
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/"; //jei portas kitas, reikia pasikeisti
        // url points to jdbc protocol : mysql subprotocol; localhost is the address
        // of the server where we installed our DBMS (i.e. on local machine) and
        // 3306 is the port on which we need to contact our DBMS
        
        // String driver = "com.mysql.jdbc.Driver";
        
        //BŪTINAI spausti ant projekto Properties, Libraries, add JAR, addint iš dėstytojo depositor draiverį.
        
        String dbName = "vcs_2017";
        String userName = "root";
        String password = "";
        Connection conn = null;

        try {                       //kreipsimės į duomenų bazę, todėl gali būti klaidų
            conn = DriverManager.getConnection(url + dbName, userName, password);
            out("Valio, prisijungėme prie database. schema: " + conn.getSchema());
            conn.getMetaData();
            Statement s = conn.createStatement();
            s.executeUpdate("insert into person values("
                            + (getLastId(s, " person ")+1)              
                            + ", 'As', 'Mano', 2, 77, 'as@mano.lt');");          //prideda dar nenaudojamą ID
            //conn.commit();
            ResultSet rs = s.executeQuery("select * from person;");
            while (rs.next()) {
                String name = rs.getString(2); //2 - column index 
                String email = rs.getString("email");
                Integer id = rs.getInt(1);
                int gender = rs.getInt("gender");
                out("name: " + name + ", \t\temail: " + email + ", \t\tid: " + id + ", \t\tgender: " + gender);
                //   \t\t sulygiuoja tekstą! :)
                         
                out("Column #2 type: " + rs.getMetaData().getColumnType(2));             // varchar = 12 tipas
                out("Column #2 name: " + rs.getMetaData().getColumnName(2));
            }
        } catch (Exception e) {
            out("Unable to reach the database: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
    }        
}


//================================ DATABASES ===================================
//RDBMS

//Create statement etc etc funkcijos

// java.sql.Statement ir java.sql.ResultSet
// void beforeFirst() - kursorius prieš visą seką
// void afterLast() - kursorius po sekos

// jei pavyko pajudėti = true, jei ne, false:
// booloean absolute(int rowNumber) - keičia poziciją nuo pradžios
// boolean relative (int rowNumber) - keičia poziciją nuo kursoriaus vietos
// boolean next() - kursorius juda pirmyn
// boolean previous() - juda atgal
