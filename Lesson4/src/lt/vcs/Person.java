package lt.vcs;

import static lt.vcs.VcsUtils.random;

public class Person {
    
    //KINTAMIEJI
    private String name;
    private String surname;
    protected Gender gender;
    private int age;
    
    //KONSTRUKTORIUS
    public Person(String name){
        this.name =  name == null ? name :  name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
    }
    
    public Person(String name, Gender gender){
        this(name);
        this.gender = gender;
    }
       
    public Person(String name, String surname, Gender gender, int age){
        this(name, gender);             //kodo optimizavimas
        this.surname = surname;
        this.age = age;
    }
    
     
    //OVERRIDE
    @Override
    public String toString(){
        return getClass().getSimpleName()+ "(name=" + getName() + " gender=" + gender.getEnLabel() + ")";
    }
    
    // GETTERIAI
    public String getName(){
        return this.name;
    }
     
    public String getSurname(){
        return this.surname;
    }
    
    public Gender getGender(){
        return this.gender;
    }
    public int getAge(){
        return this.age;
    }
  
    // SETTERIAI
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    
}
