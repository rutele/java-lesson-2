package lt.vcs;
import static lt.vcs.VcsUtils.*;

public class Player extends Person {
   
    //KINTAMIEJI
    private int dice;  // Alt+Insert - sugeneruoti kodą, pvz getterį

    //KONSTRUKTORIUS
    public Player(String name){
        super(name);        //importina iš tėvinės klasės
        this.dice = random(1,6); 
    }
    
    public Player(String name, Gender gender){
        this(name);
        this.gender = gender; //svarbu, kad ne private, o protected.
    }
    
    public Player(String name, String surname, Gender gender, int age){
        this(name, gender);
        setSurname(surname);
        setAge(age);
    }
    
    //METODAS
    public String toString(){
        return super.toString().replaceFirst("\\)", " dice=" + dice +"\\)");
        // super - kreipiasi į hierarchiją
        //.toString - kreipiasi į metodą
        //.replaceFirst - pirmą rastą skliaustelį pakeitė į stringą
    }
    
    //GETTERIAI / SETTERIAI 
    public int getDice() {
        return this.dice;
    }    
}
