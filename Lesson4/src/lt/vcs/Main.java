package lt.vcs;
import static java.lang.reflect.Array.set;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static lt.vcs.VcsUtils.*;

public class Main {

    public static void main(String[] args) {
        Player p1 = new Player (inWord("Enter name"), Gender.getById(inInt("Enter gender")));
// kodo eilutės trumpa dalis ATITINKA 4 ilgas eilutes:        
//        String name= inWord ("Enter name");
//        int g = inInt ("Enter gender");
//        Gender gen = Gender.getById(g);
//        out(new Player (name, gen));
        Person per1 = new Person("zmogeliukas");
        per1 = p1; //person'ui _priskiriama_ reikšmė iš aukščiau medyje esančio objekto (child) - player
        Object obj = p1; //same as above
        out(p1);
        out(per1);
        out(obj);
        Object [] masyvas = {p1, per1, obj, "String pvz"};
         

        User <String> u1 = new User("Vartotojas");
        User <Player> u2 = new User(p1);
        out(u1.getPerson()); //padėjus tašką pasiekiami visi string klasės metodai
        out(u2.getPerson()); //padėjus tašką pasiekiami Player klasės metodai
       
        
        List <User<String>> lst = new ArrayList();
        
        //POLIMORFIZMAS - sujungta hierarchija, vienas turi kelių požymius.
    
    //DEIMANTINĖ SINTAKSĖ (nes <> viduje)
    //klasė gali turėti parametrus.
    
    //KOLEKCIJOS
    //skirtos patogiai laikyti duomenų rinkinius. dažniausiai naudojami metodai:
    //list turi: add, addAll, contains (returns T/F), containsAll (T/F), equals,
    //    isEmpty (T/F), remove (pagal index/value, removeAll, iterator, 
    //    clear, get (pagal index), indexOf (pagal objekto pav), replaceAll,
    //    size (=kiek elementų kolekcijoje), sort, set (į indexo vietą, kokį objektą), 
    //    sublist (nuo index x iki index y), toArray() - paversti į masyvą
    //set turi: beveik tą patį, ką listai
    
    
    // INTERFEISAI
    //Set - nėra besidubliuojančių reikšmių. unikalių objektų kolekcija
    //List - gali būti dubliuojančių, duomenis galima rūšiuoti (asc/dsc)
        //ArrayList - naudojamiausia 
    //Queue - sudėti objektai išlaiko eilės tvarką (FIFA)
        //LinkedList - išimtis
    //Maps
    
    out("--------listas-------------");
    List<String> strList = new ArrayList();
    strList.add("bla");
    strList.add("bla");
    for (String bla : strList){
         out(bla);
        }
    out("--------setas-------------");
    Set<String> strSet = new HashSet();
    strSet.add("bla");
    strSet.add("bla");
    for (String bla : strSet){
         out(bla);
        }
    
    out("--------debuginimai-------------");
    if (strSet.size() <1){
        throw new RuntimeException("You've got an error!");
    }
    
    }        
}
