package lt.vcs;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static lt.vcs.VcsUtils.*;


public class Main {
    
    private static void outCollection (Collection col){
        if (col != null){
            for (Object item : col){
                out(item);
            }   
        }
    }
    
    public static void task1(String entry) {
        String up = entry.toUpperCase();                                        //didžiosios
        String str = up.replaceAll(" ", "");                                    //pašalinti tarpelius
        Map<Character, Integer> numChars = new HashMap<>();                     //hashmapas
        for (int i = 0; i < str.length(); ++i) {                                //paima vieną simbolį
            char charAt = str.charAt(i);
            if (!numChars.containsKey(charAt)) {
                numChars.put(charAt, 1);                                        //jei hashmapas neturi tokio char, jį prisideda
            } else {
                numChars.put(charAt, numChars.get(charAt) + 1);                 //jei jau turi, simbolį pasižymi skaičių +1
            }
        }
        for (char key : numChars.keySet()) {
            out(key + " - " + numChars.get(key));
        }
    }
    
    
    public static void task2(String entry) {
        
        //Paimti sakinį:
        String up = entry.toUpperCase();                                        //didžiosios
        String str = up.replaceAll(" ", "");                                    //pašalinti tarpelius
        Map<Character, Integer> numChars = new HashMap<>();                     //hashmapas
        for (int i = 0; i < str.length(); ++i) {                                //paima vieną simbolį
            char charAt = str.charAt(i);
            if (!numChars.containsKey(charAt)) {
                numChars.put(charAt, 1);                                        //jei hashmapas neturi tokio char, jį prisideda
            } else {
                numChars.put(charAt, numChars.get(charAt) + 1);                 //jei jau turi, simbolį pasižymi skaičių +1
            }
        }
        
        //Paimti norimas raides:
        out("Please enter the desired letters in uppercase, separated by commas");
        out("To finish adding letters please enter 0");
        List<String> letters = new ArrayList();
        int j = 0;
        for (j=0; ; j++){
            String x = inWord();
            letters.add(x);
            if(x.equals("0")){
                break; 
            }
        }
//        Collections.sort(letters);
//        outCollection(letters);

//Suskaiciuotas raides isvesti mazejimo tvarka pagal skaiciu kiek kartu jos buvo panaudotos 
        
        for (String strChar : letters) {
            Character charas = strChar.charAt(0);
            for (char key : numChars.keySet()) {
                if (charas.equals(key)) {
                    
                    out(key + " - " + numChars.get(key));
                }
            }
        }                    
   }
    
    public static void main(String[] args) {
        out("Please choose an app:");
        out("1 - ananasas");
        out("2 - sakinys");
        int task = inInt(); 
        switch (task){
            case 1:
                task1(inLine("Enter a sentence: "));
                break;
            case 2:
                task2(inLine("Enter a sentence: "));
                break;
            default:
                out("Entry not valid");
                break;
        }
    }  
}

