package lt.vcs;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class VcsUtils {
    
    private static String timeNow(){
        SimpleDateFormat sdf = new SimpleDateFormat("'['HH:mm:ss']'");
        return sdf.format(new Date());  //nuskaito dabartinį laiką
    }
    
    public static void out(Object txt) {
        System.out.println(timeNow() + " " + txt); //išspausdina tekstą ir parašo laiką
    }
    
    private static Scanner newScan(){ //privatus, nes nekviečiamas outside klasės.
        return new Scanner(System.in); 
    }
     
    public static int inInt(){
        return newScan().nextInt();         //nuskenuoja int
    }
    
    public static int inInt(String txt){    //overload - string
        out (txt);
        return inInt();     
    }
    
    public static String inWord(){
        return newScan().next();           //nuskenuoja word
    }
    
    public static String inWord(String txt){  //overload - string
        out (txt);
        return inWord();
    }
        
    public static String inLine(){
        return newScan().nextLine();       //nuskenuoja line
    }
    
    public static String inLine(String txt){  //overload - string
        out (txt);
        return inLine();
    }
}
