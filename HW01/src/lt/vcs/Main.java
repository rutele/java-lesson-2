package lt.vcs;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
     
                //TASK1
    private static void out(String txt) {
        System.out.println(txt);
    }
    private static Scanner newScan(){
        return new Scanner(System.in); //grąžinti naują scanner objektą
    }
    private static int inInt(){
        return newScan().nextInt(); //priimti sveiką skaičių
    }
    public static String inLine(){
        return newScan().nextLine();       //nuskenuoja line
    }
       // int inputInt = sc.nextInt(); //int
       // String inputWord = sc.next(); //a word
       // String inputLine = sc.nextLine(); //a line
      
    private static void task2 () {
        out("Please enter a number from 1 to 10:");
        int number = inInt(); 
        if ((number >= 1) && (number <5)) {
            out("Number is less than 5."); }
        else if (number == 5) {
            out("Number is equal to 5.");}
        else if ((number > 5) && (number <= 10)) {
            out("Number is greater than 5.");}
        else if ((number <1) || (number > 10)){
            out("The entry is not valid");
            Main.task2();} 
    }   
    
    private static void task3 () {
    out("Please enter the first number:");
        int number1 = inInt();  
    out("Please enter the desired operator number:");
        int operator = inInt(); 
    out("Please enter the second number:");
        int number2 = inInt(); 
    int result;
    switch (operator){
        case 1:
            result = number1 + number2;
            out(number1 + " + " + number2 + " = " + result);
            break;
        case 2:
            result = number1 - number2;
            out(number1 + " - " + number2 + " = " + result);
            break;
        case 3:
            result = number1 * number2;
            out(number1 + " * " + number2 + " = " + result);
            break;
        case 4:
            if (number2 == 0) {
                out("You cannot divide by zero.");}
            else {
                result = number1 / number2;
                out(number1 + " / " + number2 + " = " + result);}
            break;
        default:
            out("You have entered invalid operator.");}
    }
    
    private static void task4 () {
        out("Please enter a word:");
        Scanner sc = new Scanner(System.in);
        String word = sc.next();
        int length = word.length();
        String WORD = word.toUpperCase();
        out(WORD + " - " + length + " letters.");
    }
        
    private static void task5 () {
        out("Please enter some integers.");
        out("To finish the entering and to calculate the sum please enter 0");
        List <Integer> skaiciai = new ArrayList();
        int i = 0;
        int sum = 0;
        for (i=0; ; i++){
            int x = inInt();
            skaiciai.add(x);
            if(x == 0){
             break; 
            }
        }
        for(i=0; i < skaiciai.size(); i++){
            System.out.print(skaiciai.get(i)); 
            sum += skaiciai.get(i);
            if (i < skaiciai.size() - 1){
                System.out.print(" + ");
            } 
        }
        out(" = "+sum);
    }
    
    private static void task6 () { 
        while (1 == 1){
            out("Please enter a word:");
            Scanner sc = new Scanner(System.in);
            String entry = sc.nextLine(); 
            if (entry.equals("stop"))
                break;
            out(entry);
        }
    }
    
    private static void task7 () {
        out("Please enter a number between 1 and 10:");
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt(); 
        for (int i = 1; i <= x; i++){
            for (int j = 1; j <= 10; j++){
            out(i + " * " + j + " = " + (i*j) + "  " );
            }
            System.out.println();
        }
    }
     
    private static void task8 () {
        out("Please enter a sentence");
        String sentence = inLine();
        String sentenceCap = sentence.toUpperCase();
        
        out("Please enter a desired letter");
        String x = inLine();
        String xCap = x.toUpperCase();
        
        int length = sentenceCap.length(); //sakinio ilgis
        String lengthMinus = sentenceCap.replaceAll(xCap, "");
        int num = length - lengthMinus.length();
        out("There was (or were): " + num  + " " + xCap + " letters.");
    }
          
    private static void task9 () {
        out("Please enter a sentence");
        String sentence = inLine();
        String str = sentence.toUpperCase(); //didžiosios
        str = str.replaceAll(" ", "");      //pašalinti tarpelius
        Map<Character, Integer> numChars = new HashMap<>(); //hashmapas
        for (int i = 0; i < str.length(); ++i){ // paima vieną simbolį
            char charAt = str.charAt(i);
            if (!numChars.containsKey(charAt)){
                numChars.put(charAt, 1);        // jei hashmapas neturi tokio char, tada jį prisideda
            }
            else{
                numChars.put(charAt, numChars.get(charAt) + 1);  //jei jau turi, simbolį, pasižymi skaičių +1
            }
        }
        out(""+numChars);     //išrašo visą hashmapą, easy, jėėėė 
     }
    
  
     public static void main(String[] args) {
        out("Please choose an app:");
        out("2 - number evaluation");
        out("3 - calculator");
        out("4 - letters in word");
        out("5 - sum until zero");
        out("6 - stop words");
        out("7 - multiplication table");
        out("8 - sentence");
        out("9 - letter calculator");
        
        int task = inInt(); 
        switch (task){
            case 2:
                Main.task2();
                break;
            case 3:
                Main.task3();
                break;
            case 4:
                Main.task4();
                break;
            case 5:
                Main.task5();
                break;
            case 6:
                Main.task6();
                break;
            case 7:
                Main.task7();
                break;
            case 8:
                Main.task8();
                break;
            case 9:
                Main.task9(); //nėra
                break;
            default:
                out("You have entered an invalid number.");
                break;
        }            
    }
}
