package lt.vcs;
import java.util.Arrays;
import java.util.Scanner;

//===== MASYVAI ================================================================
    //dataType [] arrayRefVar; - deklaracija
    //dataType [] arrayRefVar = new datatype[arraySize]; - instanciacija
    //dataType [] arrayRefVar = {value0, value1, ..., valueN}; - reikšmių priskyrimas
    //java.util.Arrays klasė.
        
//===== GITHUB / SOURCETREE / BITBUCKET=========================================    
    //Galima matyti failų versijas. Registruojami pakitimai
    //GIT komandos:
    //git clone     parsisiųsti (nusiklonuoti) repozitorių iš serverio į PC
    //git add       į šusnį surinkti pakeitimus PC lyginant su serveriu. git add all = stage all
    //git commit    lokaliai nusiųsti pakeitimą iš PC į serverį. pažymėti git commit "pakeitimai"
    //git push      nusiųsti commitus iš PC į serverį.
    //git fetch     sinchronizuoti repozitoriaus serverio versiją su PC versija.
    //git pull      atnaujinti failus branch'e į naujausią serverio versiją.
    
public class Main {

    private static void out(String txt) {
        Scanner sc = new Scanner(System.in); // Alt+Enter, import java.util.Scanner;
        System.out.println(txt);
    }
     
    private static Scanner newScan(){
        return new Scanner(System.in); //grąžinti naują scanner objektą
    }
     
    private static int inInt(){
        return newScan().nextInt(); //priimti sveiką skaičių
    }
    
    private static int sumuok(int... skaiciai){ // int negali būti null, Integer gali.
        // parametro su daugtaškiu vieta turi būti paskutinė metodo sąraše
        int result = 0;                          
        if (skaiciai != null) {
   
        }
        return result;
    }
          
     
    public static void main(String[] args) {
        out("Please enter a number"); //Ctrl+Space, parodo variantus, ką galime rašyti.
        int numEntry = inInt();
        out(Integer.toString(numEntry));
        
        //out(numEntry + " "); // string geriau dėti pradžioje
        //out(newScan().toString()); //meta error
        
        //String outas = "Entry: %d"; // d - decimal
        //out(String.format(outas, numEntry));
        
       
        String [] mass = new String[5];
        String [] mass2 = {"Hi", "Hello", "Hiya"};
        out(" " + mass.length); // tarpas convertina į stringą
        //out("" + 7 + 5); - outputs 75, nes čia stringas.
        out(" " + mass2.length);
        out(" " + mass2[1]);
        mass2[1] = "O hi";
        out("" + mass2[1]);
        for (int i = 0; i < mass.length; i++){
            out(mass2[i]);
        }
        
        sumuok(6, 5, 4, 3, 2); // begalybė kintamųjų
        
        int[] mass3 = {1, 2, 3, 4};
        sumuok (mass3);
        
    }
}
