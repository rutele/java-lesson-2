package lt.vcs;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.PersonService.*;

public class Main {

    public static void main(String[] args) {
        
        Person p1 = new Person("John", "Smith", 0, 30, "john@smith.com");
    
        PersonService.save(p1);
        
    }        
}