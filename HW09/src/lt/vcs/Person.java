package lt.vcs;

public class Person {
    
    //KINTAMIEJI
    private int id;
    public static String name;
    public static String surname;
    public static int gender;
    public static int age;
    public static String email;

    //KONSTRUKTORIUS
    public Person(String name, String surname, int gender, int age, String email){
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.age = age;
        this.email = email;
    }
    
    //GETTERIAI
    public int getId() {
        return age;
    }
    public String getName(){
        return this.name;
    }
    public String getSurname(){
        return this.surname;
    }
    public int getGender(){
        return this.gender;
    }
    public int getAge(){
        return this.age;
    }
    public String getEmail() {
        return email;
    }
}
