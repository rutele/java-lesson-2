package lt.vcs;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.*;

public class PersonService {
    
    
    private static Integer getLastId(Statement s, String table){
        Integer result = null;
        try {
            ResultSet rs = s.executeQuery("select id from " + table + " order by id desc limit 1;");
            if(rs.next()){
                result = rs.getInt(1);
            }
        } catch (Exception e) {
            out("Error while retrieving the last id from " + table + ": " + e.getMessage());
        }
        return result;
    } 
    
    public static boolean save(Person person) {
        boolean result = false;
        
        String url = "jdbc:mysql://localhost:3306/";
        //String driver = "com.mysql.jdbc.Driver";
        String dbName = "vcs_2017";
        String userName = "root";
        String password = "";
        Connection conn = null;

        try {                       
            conn = DriverManager.getConnection(url + dbName, userName, password);
            out("Valio, prisijungėme prie database. schema: " + conn.getSchema());
            conn.getMetaData();
            
            
            Statement s = conn.createStatement();
            s.executeUpdate("insert into person values(" + (getLastId(s, " person ")+1) +              
                    ", '" + Person.name + "', '"+ Person.surname +"', " + 
                    Person.gender + ", " + Person.age +", '" + Person.email +"');");     
            //ResultSet rs = s.executeQuery("select * from person;");
            result = true;
            
        } catch (Exception e) {
            out("Unable to reach the database: " + e.getMessage());
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
        
        out(""+ result);
        return result;
        
        }
}
