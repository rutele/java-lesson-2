package lt.vcs;

public enum Gender implements Named { 
    FEMALE("Female", "Moteris", 0),
    MALE("Male", "Vyras", 1),
    OTHER("Other", "Kita", 2);
    
    private String enLabel;
    private String ltLabel;
    private int id;
    
    private Gender (String enLabel, String ltLabel, int id){
        this.enLabel = enLabel;
        this.ltLabel = ltLabel; 
        this.id = id;
    }
    
    public static Gender getById(int id){
        for(Gender gen : Gender.values()){
            if (id == gen.getId()){
                return gen;
            }
        }
        return null;
    }
       
    public String getEnLabel() {
        return enLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }
    
    @Override
    public int getId() {
        return id;
    }
    
    @Override
    public String getName() { //nėra throws, nes jis jau deklaruotas
        return null;
    }
}
