package lt.vcs;
import static lt.vcs.VcsUtils.*;

public abstract class AbstractThingy implements Idable{ //nereikia override getId, nes nėra
    // ABSTRACT CLASSES:
    //gali turėti griežtai apibrėžtus metodus
                //konkrečias metodo implementacijas
    //negali būti inicializuojamos
    //negali turėti privačių objektų
    
    public abstract Object naujasObjektas();
    
//    public int getId(){                            galima implementuoti čia
//        return 55;
//    }
    
    public void bendrasFunkcionalumas(){
        out("" + naujasObjektas() + getId());
    }
    
    
}
