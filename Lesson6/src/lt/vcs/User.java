package lt.vcs;

/**
 * @param <T> person tipo parametras
*/

public class User <T>{
    //T - bet kokie
    
    //priverstinai Person tipo kintamasis:
    //public class User<T extends Person>{
    // u1 tampa klaida (nes jis string)
    
    //KINTAMIEJI
    private T person;
    
    public User(T person){
        this.person = person;
    }
    
    public T getPerson(){
        return person;
    }
    
}
