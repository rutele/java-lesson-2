package lt.vcs;
import static java.lang.reflect.Array.set;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static lt.vcs.VcsUtils.*;

public class Main {

    //===============OBJEKTINIS PROGRAMAVIMAS===================================
    //Polymorphism
    //Inheritance
    //Encapsulation
    //Abstraction - taisyklė, kurią turi įvykdyti tam tikros klasės
    //Classes - visas apibrėžiama klasėse
    //Objects - klasėse kuriami objektai
    //Instance - esybės, objektai
    //Method
    //Message Parsing - threads interact, multithreading, kol gijoje vyksta darbas, galima dirbti kitoje gijoje.
    
    //=========================INTERFEISAI======================================
    //vieną interface gali realizuoti daug klasių
    //viena klasė gali implementuoti daug interfeisų
    //viena klasė gali extendent tik vieną klasę
    //interfeisai yra klasės, kuriose negalime kurti objektų, deklaruoti konstruktorių.
    //interfeisai yra nurodymų rinkinys
    //galima kurti objektus klasėse, kurios implementuoja interfeisą
    
    
    //visi, kas nėra static, yra abstract
    
    
    public static void main(String[] args) {
        Named g1 = Gender.FEMALE;
        Named g2 = null;        //deklaruojame, kad pasiektumėme už try ribų.
        try {
            g2 = new Person("John", "Smith"); //
        } catch (Exception e){ 
        }
        List<Named> namedList = new ArrayList();
        namedList.add(g1);
        namedList.add(g2);
        for (Named named :namedList){
            try {
            out(named.getName());
                if (named instanceof Idable){
                Idable idable = (Idable) named;
                out(idable.getId());
                }
            } catch (Exception e){ 
              }
          
        }
        out("Anything one");
        AbstractThingy ad1 = new Anything();
        ad1.bendrasFunkcionalumas();
        
        out("Anything two");
        AbstractThingy ad2 = new Anything2();
        ad2.bendrasFunkcionalumas();
        
    }
}
