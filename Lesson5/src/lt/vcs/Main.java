package lt.vcs;
import static java.lang.reflect.Array.set;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static lt.vcs.VcsUtils.*;

public class Main {

    private static Set<String> createSet (String... strings) {
        Set<String> result = new HashSet();
        if (result != null){
            result.addAll(Arrays.asList(strings)); 
            //grąžina Hashset'ą, užpildytą duomenimis iš parametro, deklaruoto metode
        }
        return result;
    }
    
    private static List<String> createList (String... strings) {
        List<String> result = new ArrayList();
        if (result != null){
            result.addAll(Arrays.asList(strings));
        }
        return result;
    }
    
    private static void outCollection (Collection col){
        if (col != null){
            for (Object item : col){
                out(item);
            }   
        }
    }
    
    private static void listsSetsMaps(){
        //===================== LIST & SET =====================================
        String line = inLine("Enter any words separated by commas");
        line = line.replaceAll(" ", ""); //tarpelių ištrynimas
        String [] lineMas = line.split(","); //išskaido per kablelius
        List<String> lineList = createList(lineMas);
        Set<String> lineSet = createSet(lineMas);
        
        out("===========LineList===============");// linked list - jei reikia įvedimo eilės tvarka 
        outCollection(lineList);
        out("===========LineSet==============="); //set'e nebus vienodų reikšmių, eilės tvarka random
        outCollection(lineSet);
        out("===========LineList SORTED=======");
        Collections.sort(lineList);
        outCollection(lineList); 
        out("===========LineList SORTED REVERSE");
        Collections.reverse(lineList);
        outCollection(lineList);
        
        //for(String str :lineList){  
        //    lineList.remove(str);         //išmeta visus stringus iš listo.
        //}                                 //nesaugus išmetimas, nes gali sujaukti eilę. geriau naudoti:
        
        //======================ITERATORIUS=====================================
        Iterator<String> iter = lineList.iterator(); // listą išsiskleidžia į eilutę ir eina eiliškai per narius
        while (iter.hasNext()){             //kol turi sekantį narį, ...
           String iterItem = iter.next();   //patikrina žymeklio poziciją
           iter.remove();                   //išima narį
        }
        
        //=====================MAP INTERFACE====================================
        Map<String, Integer> mapas = new HashMap(); 
        //map - kaip dictionary <keys (dabar stringai), values (bus integers)>
        mapas.put("obuolys", 5);
        Integer val = mapas.get("obuolys");
        out(val);
        Set<String> raktai = mapas.keySet();
        for (String raktas : raktai){           //galima iteruoti per seto reikšmes
            //do smth
        }
        Collection<Integer> reiksmes = mapas.values();
        for (Integer reiksme : reiksmes){           //galima iteruoti per kolekcijos reikšmes
            //do smth
        }
        //mapas.entrySet();
    }
    
    private static void exceptions(){
        
        //===========================EXCEPTIONS=================================
        //ERROR - pvz neužtenka atminties. rimtos, sisteminės klaidos. jų gaudyti nereikia.
        //EXEPTION (checked) - pvz neranda nurodyto failo. galima apdoroti.
        //RUNTIMEEXEPTION (unchecked) - galima gaudyti, nėra ko apdoroti.
        
        //Handling: try{}catch(Exception e){}
    }
    
    public static void main(String[] args) {
        
       // listsSetsMaps();
        
       
        Player p1 = null;
        while (p1 == null){
            try{
                new Player(inWord("Name"), inWord("Email"));
            }catch (Exception e){           //e - kintamojo reikšmė
                out(e.getMessage());
            };
        }
        
        try{
            Person per1 = new Person("", "");
        } catch (BadDataInputException | OffensiveDataInputException e){ //pirma sugaudome vaikų klasę, paskui tėvinę
            out(e.getMessage());
        } catch (Exception e){      //bendrinis, sugauna visus exceptionus
            out(e.getMessage());
        }
     
        
        
    //==========================DEBUG==========================================
    //paspausti ant norimos eilutės nr (tampa raudona)
    //right click, debug filo or project
    //atsidaro Debug View, sustoja prieš įvykstant raudonai eilutei
    }
}


