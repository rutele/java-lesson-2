package lt.vcs;

import static lt.vcs.VcsUtils.random;
import java.io.IOException;

public class Person {
    
    //KINTAMIEJI
    private String name;
    private String surname;
    protected Gender gender;
    private int age;
    private String email;

    //KONSTRUKTORIUS
    public Person(String name, String email) throws BadDataInputException, OffensiveDataInputException { //deklaruojame klaidą
        if (name == null || name.trim().length() == 0 || "vardenis".equals(name)) {
            // ARBA UNCHECKED EXCEPTION: 
            // throw new NullPointerException("Email cannot be null"); 
            // tuomet nereikia throws Exception priekyje (deklaravimas)
            throw new BadDataInputException("name must be not null"); //CHECKED EXCEPTION
        } else {
            this.name = name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        }
        if (email == null || email.trim().length() == 0 || "vardenis".equals(email)) {
            throw new OffensiveDataInputException("email must be not null");
        } else {
            this.email = email;
        }
    }

// this.name =  name == null ? name :  name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
 
        

    public Person(String name, String email, Gender gender) throws Exception {
        this(name, email);                      //kodo optimizavimas
        this.gender = gender;
    } 
    public Person(String name, String surname, String email, Gender gender, int age) throws Exception{
        this(name, email, gender);             //kodo optimizavimas
        this.surname = surname;
        this.age = age;
    }
    
     
    //OVERRIDE
    @Override
    public String toString(){
        return getClass().getSimpleName()+ "(name=" + getName() + " gender=" + gender.getEnLabel() + ")";
    }
    
    
    // GETTERIAI
    public String getName(){
        return this.name;
    }
    public String getSurname(){
        return this.surname;
    }
    public Gender getGender(){
        return this.gender;
    }
    public int getAge(){
        return this.age;
    }
    public String getEmail() {
        return email;
    }
  
        
    // SETTERIAI
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setAge(int age) {
        this.age = age;
    }
    
    
}
