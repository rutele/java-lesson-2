package lt.vcs;
import static lt.vcs.VcsUtils.*;

public class Player {
   
    //KINTAMIEJI
    private String name;
    private int dice;
    private Gender gender;
    private int[] diceHand;


    //KONSTRUKTORIUS
    public Player(String name, Gender gender){     //kai deklaruojame, prarandame defaultinį konstruktorių
                                                   //jei reikia defaultinio, naudoti dar ir tuščią konstruktorių
        this.name =  name == null ? name :  name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase();
        // ? apsaugo, jei būtų įvestas null.
        //substring(0, 1)  - įeina nuo pirmo elemento iki antro, jo neįtraukiant.
        //substring - grąžina naują stringą
        //Character.toUpperCase(name.charAt(0)) + (name.substring(1)).toLowerCase(); 
        this.dice = random(1,6); 
        this.gender = gender;
        int[] penki = {random(1,6), random(1,6), random(1,6), random(1,6), random(1,6), random(1,6)};
        this.diceHand = penki;
    }                                   
    
    //GETTERIAI / SETTERIAI
    public String getName(){ 
        return this.name;
    }
    public int getDice() {
        return this.dice;
    }
    public Gender getGender() {
        return this.gender;
    }
    public int[] getDiceHand() {
        return this.diceHand;
    }
}
