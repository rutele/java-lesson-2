package lt.vcs;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import static lt.vcs.VcsUtils.*; //statiškas importas, import all!
import static lt.vcs.Player.*;
public class Main {
    
    public static void lecturepart1(){
        out("Enter a three letter word");
        String abc = inWord();
        String xyz = inWord();
        if(abc != null && abc.equals(xyz)){ //išvengti erroro
            out("abc for sure!!!");}
        else{
            out("Not abc");}
        
        String result = abc != null && abc.equals(xyz) ? "abc for sure!!!" : "Not abc";
        out (result);
        //Ctrl+Shift+1 - parodo, kurioje klasėje esame.
        
        out (new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("'Data: 'yyyy-MM-dd 'Laikas: ' HH:mm:ss");
        out(sdf.format(new Date()));
        
        out(random(1,10));
        }
    
    public static void onedice(){
        out("Welcome to the dice game!");
        
        //PLAYER 1
        out("Please enter name for first user:");
        out("Please enter first player's gender:");
        out("(0 - Female, 1 - Male, 2 - Other)");
        Player player1 = new Player(inWord(), Gender.getById(inInt()));
        int d1 = player1.getDice();
        String p1 = player1.getName();
        
        //PLAYER 2
        out("Please enter name for second user:");
        out("Please enter second player's gender:");
        out("(0 - Female, 1 - Male, 2 - Other)");
        Player player2 = new Player(inWord(), Gender.getById(inInt()));
        int d2 = player2.getDice();
        String p2 = player2.getName();      

         //THROW DICE ONCE 
        out("" + p1 + " got " + d1 + ". " + p2 + " got " + d2 + ". ");
        if (d1 > d2){
            out(""+ p1 + " wins");
            }
        else if (d1 == d2){
            out ("No winner!");
        }
        else {
            out(""+ p2 + " wins");
            }
    }
    
    public static void fivedice(){
         out("Welcome to the FIVE dice game!");
        
        //PLAYER 1
        out("Please enter name for first user:");
        out("Please enter first player's gender:");
        out("(0 - Female, 1 - Male, 2 - Other)");
        Player player1 = new Player(inWord(), Gender.getById(inInt()));
        String p1 = player1.getName();
        int[] h1 = player1.getDiceHand();
        int sum1 = 0;                          
        for(int i=0; i<5; i++){
            sum1 += h1[i];                   
        }
        
        //PLAYER 2
        out("Please enter name for second user:");
        out("Please enter second player's gender:");
        out("(0 - Female, 1 - Male, 2 - Other)");
        Player player2 = new Player(inWord(), Gender.getById(inInt()));
        String p2 = player2.getName(); 
        int[] h2 = player2.getDiceHand();
        int sum2 = 0;                          
        for(int i=0; i<5; i++){
            sum2 += h2[i];                   
        }

        //THROW DICE FIVE TIMES RESULTS
        out("" + p1 + " got " + Arrays.toString(player1.getDiceHand()) + ", total sum is " + sum1);
        out("" + p2 + " got " + Arrays.toString(player2.getDiceHand()) + ", total sum is " + sum2);
        if (sum1 > sum2){
            out(""+ p1 + " wins");
            }
        else if (sum1 == sum2){
            out ("No winner!");
        }
        else {
            out(""+ p2 + " wins");
            } 
    }
    
    public static void jega2() {
        out("Pasirikite statymo sumą nuo 1 iki 10 EUR");
        int statymas = inInt();
        out("Spėkite šešis skaičius nuo 1 iki 30");
        int[] spejimas = new int[6];
        for (int i = 0; i < 6; i++) {
            out("Skaičius nr. " + (i+1));
            spejimas[i] = inInt(); 
        }
        out ("" + "Jūsų spėjimas: " + Arrays.toString(spejimas));
        
        int[] septyni = new int[7];
        for (int i = 0; i < 7; i++) {
            septyni[i] = random(1, 30);
            for (int j = 0; j < 7; j++) {
                if (i == j) {
                    septyni[i] = random(1, 30); // vis tiek ne skirtingi skaičiai
                }
            }
        }
        out ("" + "Iškritę skaičiai: " + Arrays.toString(septyni));
        
        int atspejimas = 0;  
        for (int i = 0; i < 6; i++) {       //patikrina šešis spėjimus
            for (int j = 0; j < 7; j++) {   //su septyniais iškritusiais skaičiais
            if(spejimas[i] == septyni[j]){
                atspejimas++;
            }
            }
        }
      
       int laimejimas = statymas * atspejimas;
       switch (laimejimas){
            case 3:
               laimejimas = laimejimas;
               break;
            case 4:
               laimejimas = laimejimas*10;
               break;
            case 5:
               laimejimas = laimejimas*200;
               break; 
            case 6:
               laimejimas = laimejimas*100000;
               break; 
            default:
                out("Bandykite dar kartą");
                laimejimas = 0;
       }
       out ("" + "Atspėjote skaičių: " + atspejimas);
       out ("" + "Jūsų laimejimas: " + laimejimas + "EUR.");
       
  
    }
    
    public static void main(String[] args) {
        out("Which game you want to play: 1 - one dice, 5 - five dice, 2 - \"JĖGA2\"?");
        int game = inInt();
        switch (game){
            case 1:
                    onedice();
                    break;
            case 2:
                    jega2();
                    break;
            case 5:
                    fivedice();
                    break;
            default:
                    out("Your entry is not valid.");
                    break;
        }
    }
}
