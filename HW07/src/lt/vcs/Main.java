package lt.vcs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static lt.vcs.VcsUtils.*;

public class Main {
    
    private static final String VARDAI = "C:\\Users\\User\\Documents\\Java\\vardai.txt";
    private static final String SKAICIAI = "C:\\Users\\User\\Documents\\Java\\skaiciai.txt";
    private static final String UTF_8 = "UTF-8";
    
    public static void task1(){
        
        File vardai = new File(VARDAI);
        BufferedReader br = null;
        String failoTekstas = "";
        List <String> eilutes = new ArrayList();
        String line;
        try{
            br = new BufferedReader(new InputStreamReader(new FileInputStream(vardai), UTF_8));

            while ((line = br.readLine()) != null) { 
                line = line.substring(0, 1).toUpperCase() + line.substring(1).toLowerCase();
                failoTekstas = line; 
                eilutes.add(failoTekstas);
            }
            Collections.sort(eilutes);
            for(String eilute : eilutes) {
            out(eilute);
            }   
        } catch(Exception e){
            out(e.getMessage());
        } finally{                  
           if (br != null){
               try{
                    br.close();
               } catch (Exception e){
                  out(e.getMessage()); 
               }     
           } 
        }

    }
    
    public static void task2(){
        String x = inWord("So you want to display only names starting with letter: ");
        File vardai = new File(VARDAI);
        BufferedReader br = null;
        String failoTekstas = "";
        List <String> eilutes = new ArrayList();
        List <String> eilutesX = new ArrayList();
        String line;
        int i = 0;
        try{
            br = new BufferedReader(new InputStreamReader(new FileInputStream(vardai), UTF_8));

            while ((line = br.readLine()) != null) { 
                line = line.substring(0, 1).toUpperCase() + line.substring(1).toLowerCase();
                failoTekstas = line; 
                eilutes.add(failoTekstas);
            }
            for(i=0; i<eilutes.size(); i++){
                if(eilutes.get(i).startsWith(x.toUpperCase()))
                {
                    out(eilutes.get(i));
                }
            }
        } catch(Exception e){
            out(e.getMessage());
        } finally{                  
           if (br != null){
               try{
                    br.close();
               } catch (Exception e){
                  out(e.getMessage()); 
               }     
           } 
        }

    }
    
    public static void task3(){
        File skaiciai = new File(SKAICIAI);
        BufferedReader br = null;
        String failoTekstas = "";
        List <String> eilutes = new ArrayList();
        String line;
        try{
            br = new BufferedReader(new InputStreamReader(new FileInputStream(skaiciai), UTF_8));

            while ((line = br.readLine()) != null) { 
                line = line.substring(0, 1).toUpperCase() + line.substring(1).toLowerCase();
                failoTekstas = line; 
                eilutes.add(failoTekstas);
            }
            Collections.sort(eilutes);
            for(String eilute : eilutes) {
            out(eilute);
            }   
        } catch(Exception e){
            out(e.getMessage());
        } finally{                  
           if (br != null){
               try{
                    br.close();
               } catch (Exception e){
                  out(e.getMessage()); 
               }     
           } 
        }

    }
    
    public static void main(String[] args) {
        out("Please choose an app:");
        out("1 - vardai");
        out("2 - vardai tik iš raidės x");
        out("3 - skaičiai didėjimo tvarka");
        int task = inInt(); 
        switch (task){
            case 1:
                Main.task1();
                break;
            case 2:
                Main.task2();
                break;
            case 3:
                Main.task3();
                break;
            default:
                out("Entry not valid");
                break;
        }   
    }
}
